Node Field Privacy
==================

To utilize privacy, manage the fields of a specific content type (at 
/admin/structure/types) and edit a field that you wish to hide. Check the box 
'Allow the user (editor) to hide this field's value by making it private.' and 
save the field.

When a node of this type is then edited (or created), there should be a checkbox
underneath the field with "Private". Check or uncheck as required.

This module is based on code from the User Field Privacy module 
(http://drupal.org/project/user_field_privacy) - many thanks. In theory, this
code could be adapted for any entity type.

Still to do: improve UX, by placing a padlock next to a locked field when it is
being viewed by the editor or a user with admin privileges.